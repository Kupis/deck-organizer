﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deck_Organizer
{
    class Card
    {
        public Card(SuitsEnum suit, ValuesEnum value)
        {
            Suit = suit;
            Value = value;
        }

        public SuitsEnum Suit { get; set; }
        public ValuesEnum Value { get; set; }

        public override string ToString()
        {
            return Value.ToString() + " of " + Suit.ToString();
        }
    }
}
