﻿namespace Deck_Organizer
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.deck1ListBox = new System.Windows.Forms.ListBox();
            this.moveToDeck2Button = new System.Windows.Forms.Button();
            this.moveToDeck1Button = new System.Windows.Forms.Button();
            this.deck2ListBox = new System.Windows.Forms.ListBox();
            this.reset1Button = new System.Windows.Forms.Button();
            this.reset2Button = new System.Windows.Forms.Button();
            this.shuffle1Button = new System.Windows.Forms.Button();
            this.shuffle2Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // deck1ListBox
            // 
            this.deck1ListBox.FormattingEnabled = true;
            this.deck1ListBox.Location = new System.Drawing.Point(12, 12);
            this.deck1ListBox.Name = "deck1ListBox";
            this.deck1ListBox.Size = new System.Drawing.Size(200, 329);
            this.deck1ListBox.TabIndex = 0;
            // 
            // moveToDeck2Button
            // 
            this.moveToDeck2Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveToDeck2Button.Location = new System.Drawing.Point(218, 92);
            this.moveToDeck2Button.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.moveToDeck2Button.Name = "moveToDeck2Button";
            this.moveToDeck2Button.Size = new System.Drawing.Size(88, 23);
            this.moveToDeck2Button.TabIndex = 1;
            this.moveToDeck2Button.Text = ">>";
            this.moveToDeck2Button.UseVisualStyleBackColor = true;
            this.moveToDeck2Button.Click += new System.EventHandler(this.moveToDeck2Button_Click);
            // 
            // moveToDeck1Button
            // 
            this.moveToDeck1Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveToDeck1Button.Location = new System.Drawing.Point(218, 135);
            this.moveToDeck1Button.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.moveToDeck1Button.Name = "moveToDeck1Button";
            this.moveToDeck1Button.Size = new System.Drawing.Size(88, 23);
            this.moveToDeck1Button.TabIndex = 2;
            this.moveToDeck1Button.Text = "<<";
            this.moveToDeck1Button.UseVisualStyleBackColor = true;
            this.moveToDeck1Button.Click += new System.EventHandler(this.moveToDeck1Button_Click);
            // 
            // deck2ListBox
            // 
            this.deck2ListBox.FormattingEnabled = true;
            this.deck2ListBox.Location = new System.Drawing.Point(312, 12);
            this.deck2ListBox.Name = "deck2ListBox";
            this.deck2ListBox.Size = new System.Drawing.Size(200, 329);
            this.deck2ListBox.TabIndex = 3;
            // 
            // reset1Button
            // 
            this.reset1Button.Location = new System.Drawing.Point(12, 347);
            this.reset1Button.Name = "reset1Button";
            this.reset1Button.Size = new System.Drawing.Size(200, 23);
            this.reset1Button.TabIndex = 4;
            this.reset1Button.Text = "Generate up to 10 cards";
            this.reset1Button.UseVisualStyleBackColor = true;
            this.reset1Button.Click += new System.EventHandler(this.reset1Button_Click);
            // 
            // reset2Button
            // 
            this.reset2Button.Location = new System.Drawing.Point(312, 347);
            this.reset2Button.Name = "reset2Button";
            this.reset2Button.Size = new System.Drawing.Size(200, 23);
            this.reset2Button.TabIndex = 5;
            this.reset2Button.Text = "Restore default";
            this.reset2Button.UseVisualStyleBackColor = true;
            this.reset2Button.Click += new System.EventHandler(this.reset2Button_Click);
            // 
            // shuffle1Button
            // 
            this.shuffle1Button.Location = new System.Drawing.Point(12, 376);
            this.shuffle1Button.Name = "shuffle1Button";
            this.shuffle1Button.Size = new System.Drawing.Size(200, 23);
            this.shuffle1Button.TabIndex = 6;
            this.shuffle1Button.Text = "Shuffle";
            this.shuffle1Button.UseVisualStyleBackColor = true;
            this.shuffle1Button.Click += new System.EventHandler(this.shuffle1Button_Click);
            // 
            // shuffle2Button
            // 
            this.shuffle2Button.Location = new System.Drawing.Point(312, 376);
            this.shuffle2Button.Name = "shuffle2Button";
            this.shuffle2Button.Size = new System.Drawing.Size(200, 23);
            this.shuffle2Button.TabIndex = 7;
            this.shuffle2Button.Text = "Shuffle";
            this.shuffle2Button.UseVisualStyleBackColor = true;
            this.shuffle2Button.Click += new System.EventHandler(this.shuffle2Button_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 406);
            this.Controls.Add(this.shuffle2Button);
            this.Controls.Add(this.shuffle1Button);
            this.Controls.Add(this.reset2Button);
            this.Controls.Add(this.reset1Button);
            this.Controls.Add(this.deck2ListBox);
            this.Controls.Add(this.moveToDeck1Button);
            this.Controls.Add(this.moveToDeck2Button);
            this.Controls.Add(this.deck1ListBox);
            this.Name = "mainForm";
            this.Text = "Deck Oeganizer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox deck1ListBox;
        private System.Windows.Forms.Button moveToDeck2Button;
        private System.Windows.Forms.Button moveToDeck1Button;
        private System.Windows.Forms.ListBox deck2ListBox;
        private System.Windows.Forms.Button reset1Button;
        private System.Windows.Forms.Button reset2Button;
        private System.Windows.Forms.Button shuffle1Button;
        private System.Windows.Forms.Button shuffle2Button;
    }
}

