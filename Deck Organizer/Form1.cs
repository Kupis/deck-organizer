﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deck_Organizer
{
    public partial class mainForm : Form
    {
        Deck deck1;
        Deck deck2;

        Random random = new Random();

        public mainForm()
        {
            InitializeComponent();
            ResetDeck(1);
            ResetDeck(2);
            RedrawDeck(1);
            RedrawDeck(2);
        }

        private void ResetDeck(int deckNumber)
        {
            if (deckNumber == 1)
            {
                int numberOfCards = random.Next(1, 11);
                deck1 = new Deck(new Card[] { });
                for (int i = 0; i < numberOfCards; i++)
                    deck1.Add(new Card((SuitsEnum)random.Next(4), (ValuesEnum)random.Next(1, 14)));
                deck1.Sort();
            }
            else
                deck2 = new Deck();
        }

        public void RedrawDeck(int deckNumber)
        {
            if (deckNumber == 1)
            {
                deck1ListBox.Items.Clear();
                foreach (string cardName in deck1.GetCardNames())
                    deck1ListBox.Items.Add(cardName);
                deck1ListBox.Text = "Deck number 1";
            }
            else
            {
                deck2ListBox.Items.Clear();
                foreach (string cardName in deck2.GetCardNames())
                    deck2ListBox.Items.Add(cardName);
                deck2ListBox.Text = "Deck number 2";
            }
        }

        private void moveToDeck2Button_Click(object sender, EventArgs e)
        {
            if (deck1ListBox.SelectedIndex >= 0)
                if (deck1.Count > 0)
                    deck2.Add(deck1.Deal(deck1ListBox.SelectedIndex));
            RedrawDeck(1);
            RedrawDeck(2);
        }

        private void moveToDeck1Button_Click(object sender, EventArgs e)
        {
            if (deck2ListBox.SelectedIndex >= 0)
                if (deck2.Count > 0)
                    deck1.Add(deck2.Deal(deck2ListBox.SelectedIndex));
            RedrawDeck(1);
            RedrawDeck(2);
        }

        private void reset1Button_Click(object sender, EventArgs e)
        {
            ResetDeck(1);
            RedrawDeck(1);
        }

        private void reset2Button_Click(object sender, EventArgs e)
        {
            ResetDeck(2);
            RedrawDeck(2);
        }

        private void shuffle1Button_Click(object sender, EventArgs e)
        {
            deck1.Shuffle();
            RedrawDeck(1);
        }

        private void shuffle2Button_Click(object sender, EventArgs e)
        {
            deck2.Shuffle();
            RedrawDeck(2);
        }
    }
}
